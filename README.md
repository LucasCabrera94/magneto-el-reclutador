# Magneto el reclutador

Hola que tal,
Mi nombre es Lucas Emmanuel Cabrera, este es mi examen de MercadoLibre y voy a hacer el programa para que Magneto pueda reclutar sus mutantes.

Bueno, este es un programa hecho en C# .Net Core 2.2 que cuenta con un proyect de aplicacion y otro de Test unitarios.

Lo que se logro es usar una inyeccion de dependencias para poder abstraer la vista con el usuario, en este caso el controller, de lo que es la logica de  negocio la cual lo puse en un Interactor.
Asi mismo, los test unitarios estan directamente conectados con el Interactor. Me parecio una idea copada para poder demostrar un poco mas de lo que es la separacion por capas.

En cuanto a logica de negocio, se me ocurrio separar en varias funciones para poder tener una visibilidad y entendimiento mas claro del codigo.

Ustedes van a poder clonar el repositorio de GitLab, una vez hecho esto siguen los siguientes pasos:
 - Lo abren en Microsoft Visual Studio
 - Ejecutan el programa
 - Esperando que se abra el la pantalla de su explorador de internet
 - Podran pegarle al Post de "api/mutant", en mi caso desde Postman, junto a la matriz de string. El cual le retornara false si la persona es humana o caso contrario true si es mutante. 

Para ejecutar los test unitarios, tendran que darle click derecho al proyecto y podran "ejecutar pruebas" las cuales les devolvera los resultados de las mismas.
 
Para poder probar los test, tendran que ir al proyecto de test dentro del solution en donde podran con click derecho "ejecutar las pruebas" y les apareceran los resultados de los test!

Espero poder satisfacer todas sus espectativas y proximamente poder ser parte de ustedes!

Saludos!

