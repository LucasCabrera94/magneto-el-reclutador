using Magneto_el_reclutador.Controllers;
using Magneto_el_reclutador.Interactors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestOtherCases
    {

        [TestMethod]
        public void Test3x3()
        {
            var expectedResult = false;
            string[] BaseCase = { "AAA", "AAA", "AAA" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }

        [TestMethod]
        public void Test8x8()
        {
            var expectedResult = true;
            string[] BaseCase = { "ATGCGABV", "CAGTGCAS", "TTATGTQW", "AGAAGG�L", "CCCCTAQW", "TCACTGFD", "QQQQIOPG", "IEDFGHJC" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }

        [TestMethod]
        public void TestHumanOneSequence()
        {
            var expectedResult = false;
            string[] BaseCase = { "ATGCGA", "CAGTGC", "TTATLT", "AGAAHG", "CURCTA", "TCACTG" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }
    }
}
