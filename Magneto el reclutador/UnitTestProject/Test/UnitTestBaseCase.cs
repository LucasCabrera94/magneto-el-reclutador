using Magneto_el_reclutador.Controllers;
using Magneto_el_reclutador.Interactors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestBaseCase
    {

        [TestMethod]
        public void TestBaseCase()
        {
            var expectedResult = true;
            string[] BaseCase = { "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }
    }
}
