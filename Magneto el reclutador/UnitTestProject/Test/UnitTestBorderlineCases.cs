using Magneto_el_reclutador.Controllers;
using Magneto_el_reclutador.Interactors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestBorderlineCases
    {

        [TestMethod]
        public void TestAllChartA()
        {
            var expectedResult = true;
            string[] BaseCase = { "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }

        [TestMethod]
        public void TestNotChartRepeat()
        {
            var expectedResult = false;
            string[] BaseCase = { "QWERTY", "ASDFGH", "ZXCVBN", "POIUYT", "�LKJHG", "MNBVCX" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }

        [TestMethod]
        public void TestMatrixEmpty()
        {
            var expectedResult = false;
            string[] BaseCase = {};
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }

        [TestMethod]
        public void TestCaseWithLowercaseLetters()
        {
            var expectedResult = true;
            string[] BaseCase = { "ATGCgA", "CaGTGC", "TTATgT", "AGaAGG", "CcCcTA", "TCACTG" };
            var addMutant = new MutantInteractor().IsMutant(BaseCase);
            Assert.AreEqual(expectedResult, addMutant);
        }
    }
}
