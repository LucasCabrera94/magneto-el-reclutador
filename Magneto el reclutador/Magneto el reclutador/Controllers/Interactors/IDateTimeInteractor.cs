﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magneto_el_reclutador.Controllers
{
    public interface IDateTimeInteractor
    {
        DateTime GetCurrentDate();
    }

    public class DefaultDateTimeInteractor : IDateTimeInteractor
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.UtcNow.Date;
        }
    }
}
