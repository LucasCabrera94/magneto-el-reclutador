﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magneto_el_reclutador.Controllers
{
    public interface ICurrentUserInteractor
    {
        string GetCurrentUsername(HttpContext context);
    }
}
