﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace Magneto_el_reclutador.Controllers
{
    public class CurrentUserInteractor : ICurrentUserInteractor
    {
        private readonly IHttpUserParser userParser;

        public CurrentUserInteractor(IHttpUserParser userParser)
        {
            this.userParser = userParser ?? throw new ArgumentNullException(nameof(userParser));
        }

        public string GetCurrentUsername(HttpContext context) =>
            userParser.ParseAuthorizedUserUsername(context.User);
    }

    public interface IHttpUserParser
    {
        string ParseAuthorizedUserUsername(ClaimsPrincipal principal);
    }
}
