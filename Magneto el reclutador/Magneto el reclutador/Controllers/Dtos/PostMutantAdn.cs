﻿using Magneto_el_reclutador.DataTransferObject;

namespace Magneto_el_reclutador.Controllers.Dtos
{
    public class PostMutantAdn : IAddMutantAdn
    {
        public string[] dna { get; set; }
    }
}
