﻿using System;
using Magneto_el_reclutador.Controllers.Dtos;
using Magneto_el_reclutador.Interactors;
using Microsoft.AspNetCore.Mvc;

namespace Magneto_el_reclutador.Controllers
{
    [Route("api/")]
    [ApiController]
    public class MutantController : ControllerBase
    {
        private readonly MutantInteractor mutantInteractor;

        public MutantController(MutantInteractor mutantInteractor)
        {
            this.mutantInteractor = mutantInteractor ?? throw new ArgumentNullException(nameof(mutantInteractor));
        }

        [HttpPost("mutant/")]
        public IActionResult IsMutant([FromBody]PostMutantAdn dna)
        {
            var result = mutantInteractor.IsMutant(dna.dna);
            return Ok(result);
        }
    }
}
