﻿using Magneto_el_reclutador.Controllers;
using Microsoft.AspNetCore.Http;

namespace Magneto_el_reclutador
{
    public class CurrentUserInteractorMock : ICurrentUserInteractor
    {
        public static class Constants
        {
            public const string username = "user";
        }

        public string GetCurrentUsername(HttpContext context)
        {
            return Constants.username;
        }
    }
}
