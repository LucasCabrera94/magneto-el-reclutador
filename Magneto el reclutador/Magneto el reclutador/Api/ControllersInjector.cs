﻿using Magneto_el_reclutador.Controllers;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Magneto_el_reclutador
{
    class ControllersInjector : BaseInjector
    {
        public ControllersInjector(IServiceCollection services) : base(services)
        {
        }

        public override void RegisterDependencies()
        {
            RegisterMockDependencies();          
        }

        private void RegisterMockDependencies()
        {
            RegisterInstancePerRequest<IDateTimeInteractor, DefaultDateTimeInteractor>();
            RegisterInstancePerRequest<ICurrentUserInteractor, CurrentUserInteractorMock>();
        }
    }
}
