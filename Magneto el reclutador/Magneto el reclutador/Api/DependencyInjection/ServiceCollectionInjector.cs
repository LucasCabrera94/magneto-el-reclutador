﻿using Magneto_el_reclutador.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magneto_el_reclutador
{
    class ServiceCollectionInjector
    {
        private readonly IServiceCollection _services;
        private readonly IConfiguration _configuration;

        public ServiceCollectionInjector(IServiceCollection services, IConfiguration configuration)
        {
            _configuration = configuration;
            _services = services ?? throw new ArgumentNullException(nameof(services));;
        }

        public void ResolverServices()
        {
            RegisterMockDependencies();
            InjectLocalDependencies();
        }

        private void RegisterMockDependencies()
        {
            _services.AddScoped<IDateTimeInteractor, DefaultDateTimeInteractor>();
            _services.AddScoped<ICurrentUserInteractor, CurrentUserInteractorMock>();
        }

        private void InjectLocalDependencies()
        {
            new InteractorsInjector(_services, _configuration).RegisterDependencies();
            new ControllersInjector(_services).RegisterDependencies();
            //new RepositoriesInjector(_services, RepositoriesInjector.ServicesOrigin.EFCore).RegisterDependencies();
        }

    }
}
