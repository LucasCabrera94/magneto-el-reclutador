﻿using Magneto_el_reclutador.Controllers;
using Magneto_el_reclutador.Interactors;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Magneto_el_reclutador
{
    class InteractorsInjector : BaseInjector
    {
        private readonly IServiceCollection _services;
        private readonly IConfiguration _configuration;

        public InteractorsInjector(IServiceCollection services, IConfiguration configuration) : base(services)
        {
            _configuration = configuration;
        }

        public override void RegisterDependencies()
        {
            RegisterCommonInteractors();
        }

        private void RegisterCommonInteractors()
        {
            RegisterInstancePerRequest<MutantInteractor>();
        }
    }
}
