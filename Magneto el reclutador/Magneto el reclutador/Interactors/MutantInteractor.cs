﻿namespace Magneto_el_reclutador.Interactors
{
    public class MutantInteractor
    {
        public bool IsMutant(string [] dna)
        {
            var sequenceCount = 0;

            var IsSmallMatrix = MatrixValidation(dna);

            if (!IsSmallMatrix)
            {
                return false;
            }

            sequenceCount += HorizontalValidation(dna);

            sequenceCount += VerticalValidation(dna);

            sequenceCount += DiagonalsValidation(dna);

            if (sequenceCount >= 2)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        private bool MatrixValidation(string[] dna)
        {
            if (dna.Length == 0)
            {
                return false;
            }

            foreach (var row in dna)
            {
                if (row.Length <= 4)
                {
                    return false;
                }
            }

            return true;
        }

        private int HorizontalValidation(string[] dna)
        {
            var sequenceCount = 0;
            char repeatedLetter = ' ';
            foreach (var row in dna)
            {
                var letterCounter = 1;
                for (int i = 0; i < row.Length; i++)
                {
                    var current = row[i];

                    current = char.ToUpper(current);

                    if (current != repeatedLetter)
                    {
                        repeatedLetter = current;
                    }
                    else
                    {
                        letterCounter++;
                    }
                }

                if (letterCounter >= 4)
                {
                    sequenceCount++;
                }

            }
            return sequenceCount;
        }

        private int VerticalValidation(string[] dna)
        {
            string[] result = new string[dna.Length];

            for (int i = 0; i < dna.Length; i++)
            {
                string newString = "";
                for (int j = 0; j < dna.Length; j++)
                {
                    newString += dna[j][i];
                }
                result[i] = newString;
            }

            return HorizontalValidation(result);
        }

        private int DiagonalsValidation(string[] dna)
        {
            var sequenceCount = 0;

            sequenceCount += DiagonalsRightLeftValidator(dna);
            sequenceCount += DiagonalsLeftRightValidator(dna);


            return sequenceCount;
        }

        private int DiagonalsRightLeftValidator(string[] dna)
        {
            char repeatedLetter = ' ';
            var letterCount = 1;
            var sequenceCount = 0;

            //parte de arriba diagonal + diagonal
            var lengthX = 3;
            while (lengthX != dna.Length)
            {
                var j = 0;
                letterCount = 1;
                for (int i = lengthX; i >= 0; i--)
                {
                    var current = dna[j][i];

                    current = char.ToUpper(current);

                    if (current != repeatedLetter)
                    {
                        repeatedLetter = current;
                    }
                    else
                    {
                        letterCount++;
                    }
                    j++;
                }

                if (letterCount >= 4)
                {
                    sequenceCount++;
                }

                lengthX++;
            }

            //parte de abajo de diagonal
            var lengthY = 1;
            while (lengthY != (dna.Length - 3))
            {
                var j = dna.Length - 1;
                letterCount = 1;
                for (int i = lengthY; i <= dna.Length-1; i++)
                {
                    var current = dna[i][j];

                    current = char.ToUpper(current);

                    if (current != repeatedLetter)
                    {
                        repeatedLetter = current;
                    }
                    else
                    {
                        letterCount++;
                    }
                    j--;
                }

                if (letterCount >= 4)
                {
                    sequenceCount++;
                }

                lengthY++;
            }

            return sequenceCount;
        }

        private int DiagonalsLeftRightValidator(string[] dna)
        {
            char repeatedLetter = ' ';
            var letterCount = 1;
            var sequenceCount = 0;

            //parte de arriba diagonal + diagonal
            var lengthX = 2;
            while (lengthX != -1)
            {
                var j = 0;
                letterCount = 1;
                for (int i = lengthX; i <= dna.Length-1; i++)
                {
                    var current = dna[j][i];

                    current = char.ToUpper(current);

                    if (current != repeatedLetter)
                    {
                        repeatedLetter = current;
                    }
                    else
                    {
                        letterCount++;
                    }
                    j++;
                }

                if (letterCount >= 4)
                {
                    sequenceCount++;
                }

                lengthX--;
            }

            //parte de abajo de diagonal
            var lengthY = 1;
            while (lengthY != (dna.Length - 3))
            {
                var j = 0;
                letterCount = 1;
                for (int i = lengthY; i <= dna.Length - 1; i++)
                {
                    var current = dna[i][j];

                    current = char.ToUpper(current);

                    if (current != repeatedLetter)
                    {
                        repeatedLetter = current;
                    }
                    else
                    {
                        letterCount++;
                    }
                    j++;
                }

                if (letterCount >= 4)
                {
                    sequenceCount++;
                }

                lengthY++;
            }

            return sequenceCount;
        }
    }
}
